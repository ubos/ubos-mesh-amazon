//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.amazon;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.ParseException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.ubos.importer.handler.AbstractBasicFileImporterHandler;
import net.ubos.importer.handler.ImporterHandlerContext;
import net.ubos.importer.handler.ImporterHandlerNode;
import net.ubos.model.primitives.CurrencyValue;
import net.ubos.model.primitives.IntegerValue;
import net.ubos.model.primitives.TimeStampValue;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

/**
 * Functionality common to ImporterContentHandlers that read Amazon CSV files.
 */
public abstract class AbstractAmazonCsvImportContentHandler
    extends
        AbstractBasicFileImporterHandler
{
    /**
     * Constructor.
     *
     * @param insideZipFilePattern the relative path plus filenames we can work on.
     * @param expectedHeaderLabels the names of the colums as expected in the first row
     */
    protected AbstractAmazonCsvImportContentHandler(
            String    insideZipFilePattern,
            String [] expectedHeaderLabels )
    {
        super( Pattern.compile( "[^/]+/" + insideZipFilePattern ), PERFECT );

        theExpectedHeaderLabels = expectedHeaderLabels;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double doImport(
            ImporterHandlerNode    toBeImported,
            ImporterHandlerContext context )
        throws
            ParseException,
            IOException
    {
        if( !toBeImported.canGetStream() ) {
            return IMPOSSIBLE;
        }

        double ret;

        try {
            Reader r = new InputStreamReader( toBeImported.createStream() );

            Iterator<CSVRecord> recordIter = CSVFORMAT.parse( r ).iterator();
            if( recordIter.hasNext()) {
                if( theExpectedHeaderLabels != null ) {
                    CSVRecord labels = recordIter.next();
                    if( labels.size() != theExpectedHeaderLabels.length ) {
                        throw new ParseException( String.format( "Unexpected number of columns: %d vs %d", labels.size(), theExpectedHeaderLabels.length ), 0 );
                    }
                    for( int i=0 ; i<theExpectedHeaderLabels.length ; ++i ) {
                        String label = labels.get( i );
                        if( label.startsWith( "\ufeff" )) {
                            label = label.substring( 1 );
                            if( label.startsWith( "\"" ) && label.endsWith( "\"" )) { // in this case, automatic stripping won't work
                                label = label.substring( 1, label.length()-1 );
                            }
                        }
                        if( !theExpectedHeaderLabels[i].equals( label )) {
                            throw new ParseException( String.format( "Unexpected header label: %s vs %s", theExpectedHeaderLabels[i], label ), 0 );
                        }
                    }
                }

                ret = doImport( (AmazonImporterHandlerContext) context, recordIter );
            } else {
                ret = theOkScore;
            }

        } catch( IllegalArgumentException ex ) {
            ret = IMPOSSIBLE;
        }

        return ret;
    }

    /**
     * Perform the actual import.
     *
     * @param context context for the importing
     * @param recordIter iterates over the found rows
     * @return the score
     * @throws ParseException a parsing error occurred
     * @throws IOException an I/O project occurred
     */
    protected abstract double doImport(
            AmazonImporterHandlerContext context,
            Iterator<CSVRecord>          recordIter )
        throws
            ParseException,
            IOException;

    /**
     * Helper method to parse a TimeStamp from the String in CSV file.
     * UNfortunately Amazon uses various timestamp formats, and apparently changed which it uses where over time
     * (see https://gitlab.com/accesstracker/data-access-issues/-/issues/41 )
     *
     * @param s the String
     * @return the parsed value
     */
    protected TimeStampValue parseTimeStamp(
            String s )
    {
        TimeStampValue ret = parseTimeStamp1( s );
        if( ret == null ) {
            ret = parseTimeStamp2( s );
        }
        if( ret == null ) {
            throw new IllegalArgumentException( "Failing to parse time stamp: " + s );
        }
        return ret;
    }

    /**
     * Parse a timestamp in one format.
     *
     * @param s the String
     * @return the parsed value
     */
    protected TimeStampValue parseTimeStamp1(
            String s )
    {
        // 2019-06-25 22:51:29 UTC
        Matcher m = TIMESTAMP_PATTERN1.matcher( s );
        if( !m.matches() ) {
            return null;
        }
        String year  = m.group( 1 );
        String month = m.group( 2 );
        String day   = m.group( 3 );
        String hour  = m.group( 4 );
        String min   = m.group( 5 );
        String sec   = m.group( 6 );
        String tz    = m.group( 7 ); // always appears to be UTC or not specified

        Calendar cal = new GregorianCalendar();
        cal.set( Calendar.YEAR,         Integer.parseInt( year ));
        cal.set( Calendar.MONTH,        Integer.parseInt( month ) - 1 );
        cal.set( Calendar.DAY_OF_MONTH, Integer.parseInt( day ));
        cal.set( Calendar.HOUR_OF_DAY,  Integer.parseInt( hour ));
        cal.set( Calendar.MINUTE,       Integer.parseInt( min ));
        cal.set( Calendar.SECOND,       sec == null ? 0 : Integer.parseInt( sec ));
        cal.set( Calendar.MILLISECOND,  0 );
        cal.set( Calendar.ZONE_OFFSET,  0 );

        TimeStampValue ret = TimeStampValue.create( cal );
        return ret;
    }

    /**
     * Parse a timestamp in another format.
     *
     * @param s the String
     * @return the parsed value
     */
    protected TimeStampValue parseTimeStamp2(
            String s )
    {
        // 2019-06-25 22:51:29 UTC
        Matcher m = TIMESTAMP_PATTERN2.matcher( s );
        if( !m.matches() ) {
            return null;
        }
        String month = m.group( 1 );
        String day   = m.group( 2 );
        String year  = m.group( 3 );
        String hour  = m.group( 4 );
        String min   = m.group( 5 );
        String sec   = m.group( 6 );
        String tz    = m.group( 7 ); // always appears to be UTC or not specified

        Calendar cal = new GregorianCalendar();
        cal.set( Calendar.YEAR,         Integer.parseInt( year ));
        cal.set( Calendar.MONTH,        Integer.parseInt( month ) - 1 );
        cal.set( Calendar.DAY_OF_MONTH, Integer.parseInt( day ));
        cal.set( Calendar.HOUR_OF_DAY,  Integer.parseInt( hour ));
        cal.set( Calendar.MINUTE,       Integer.parseInt( min ));
        cal.set( Calendar.SECOND,       sec == null ? 0 : Integer.parseInt( sec ));
        cal.set( Calendar.MILLISECOND,  0 );
        cal.set( Calendar.ZONE_OFFSET,  0 );

        TimeStampValue ret = TimeStampValue.create( cal );
        return ret;
    }

    protected CurrencyValue parseCurrency(
            String currencyString,
            String priceString )
    {
        priceString = priceString.replaceAll( ",", "" ); // Silly Float.parseFloat() cannot cope with commas

        CurrencyValue ret = CurrencyValue.create( Float.parseFloat( priceString ), currencyString );
        return ret;
    }

    protected IntegerValue parseInteger(
            String s )
    {
        return IntegerValue.create( Long.parseLong( s ));
    }

    /**
     * How we parse CSV files.
     */
    protected static final CSVFormat CSVFORMAT
            = CSVFormat.Builder.create( CSVFormat.DEFAULT )
            .build();

    /**
     *
     * The names of the colums as expected in the first row.
     */
    protected final String [] theExpectedHeaderLabels;

    /**
     * One pattern for timestamps in the CSV file.
     * e.g. 2019-06-25 22:51:29 UTC (as in the 20201031 format)
     */
    public static final Pattern TIMESTAMP_PATTERN1 = Pattern.compile(
            "(\\d\\d\\d\\d)-(\\d\\d)-(\\d\\d)\\s+(\\d\\d):(\\d\\d)(?::(\\d\\d))?(?:\\s+([A-Z]{3}))?" );

    /**
     * Another pattern for timestamps in the CSV file.
     * e.g. 06/24/2019 22:51:29 UTC (as in the 20220626 format)
     */
    public static final Pattern TIMESTAMP_PATTERN2 = Pattern.compile(
            "(\\d\\d)/(\\d\\d)/(\\d\\d\\d\\d)\\s+(\\d\\d):(\\d\\d)(?::(\\d\\d))?(?:\\s+([A-Z]{3}))?" );
}
