//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.amazon;

import java.io.IOException;
import java.text.ParseException;
import net.ubos.importer.handler.AbstractMultiHandlerImporter;
import net.ubos.importer.handler.ImporterHandler;
import net.ubos.importer.handler.ImporterHandlerContext;
import net.ubos.importer.handler.ImporterHandlerNode;
import net.ubos.importer.handler.remainders.RateRemaindersImporterHandler;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.model.Amazon.AmazonSubjectArea;
import net.ubos.util.logging.Log;

/**
 * Functionality common to Amazon importers.
 */
public abstract class AbstractAmazonImporter
    extends
        AbstractMultiHandlerImporter
{
    private static final Log log = Log.getLogInstance( AbstractAmazonImporter.class );

    /**
     * Constructor.
     *
     * @param name name of the Importer
     * @param contentHandlers they are tried in turn when encountering a piece of content
     * @param remaindersImporterHandler the ImporterHandler to run at the end, if any
     */
    protected AbstractAmazonImporter(
            String                        name,
            ImporterHandler []            contentHandlers,
            RateRemaindersImporterHandler remaindersImporterHandler )
    {
        super( name, contentHandlers, remaindersImporterHandler );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ImporterHandlerContext setupImport(
            ImporterHandlerNode     rootNode,
            String                  defaultIdNamespace,
            EditableHistoryMeshBase mb )
        throws
            IOException
    {
        if( defaultIdNamespace != null ) {
            MeshObjectIdentifierNamespace thisNs = mb.getDefaultNamespace();
            thisNs.addExternalName( defaultIdNamespace );
            thisNs.setPreferredExternalName( defaultIdNamespace );
        }

        MeshObject account;
        try {
            account = mb.createMeshObject( "account", AmazonSubjectArea.AMAZONACCOUNT );

        } catch( ParseException ex ) {
            log.error( ex );
            account = null;
        }

        return new AmazonImporterHandlerContext( account, rootNode.getTimeOfExport(), mb );
    }

}
