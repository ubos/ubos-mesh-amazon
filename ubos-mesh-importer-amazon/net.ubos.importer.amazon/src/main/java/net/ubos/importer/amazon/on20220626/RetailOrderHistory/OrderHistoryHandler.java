//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.amazon.on20220626.RetailOrderHistory;

import java.io.IOException;
import java.text.ParseException;
import java.util.Iterator;
import net.ubos.importer.ImporterUtils;
import net.ubos.importer.amazon.AbstractAmazonCsvImportContentHandler;
import net.ubos.importer.amazon.AmazonImporterHandlerContext;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBase;
import net.ubos.model.Amazon.AmazonSubjectArea;
import net.ubos.model.Commerce.CommerceSubjectArea;
import net.ubos.model.primitives.SelectableMimeType;
import net.ubos.model.primitives.StringValue;
import org.apache.commons.csv.CSVRecord;

/**
 *
 */
public class OrderHistoryHandler
    extends
        AbstractAmazonCsvImportContentHandler
{
    public OrderHistoryHandler(
            String insideZipFilePattern )
    {
        super(  insideZipFilePattern,
                new String[] {
                    /*  0 */ "Website",
                    /*  1 */ "Order ID",
                    /*  2 */ "Order Date",
                    /*  3 */ "Purchase Order Number",
                    /*  4 */ "Currency",
                    /*  5 */ "Unit Price",
                    /*  6 */ "Unit Price Tax",
                    /*  7 */ "Shipping Charge",
                    /*  8 */ "Total Discounts",
                    /*  9 */ "Total Owed",
                    /* 10 */ "Shipment Item Subtotal",
                    /* 11 */ "Shipment Item Subtotal Tax",
                    /* 12 */ "ASIN",
                    /* 13 */ "Product Condition",
                    /* 14 */ "Quantity",
                    /* 15 */ "Payment Instrument Type",
                    /* 16 */ "Order Status",
                    /* 17 */ "Shipment Status",
                    /* 18 */ "Ship Date",
                    /* 19 */ "Shipping Option",
                    /* 20 */ "Shipping Address",
                    /* 21 */ "Billing Address",
                    /* 22 */ "Carrier Name & Tracking Number",
                    /* 23 */ "Product Name",
                    /* 24 */ "Gift Message",
                    /* 25 */ "Gift Sender Name",
                    /* 26 */ "Gift Recipient Contact Details"
               });

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double doImport(
            AmazonImporterHandlerContext context,
            Iterator<CSVRecord>          recordIter )
        throws
            ParseException,
            IOException
    {
        MeshBase mb = context.getMeshBase();

        while( recordIter.hasNext() ) {
            CSVRecord record = recordIter.next();

            String orderIdString          = record.get(  1 );
            String orderedOnString        = record.get(  2 );
            String currencyString         = record.get(  4 );
            String unitPriceString        = record.get(  5 );
            String unitSalesTaxString     = record.get(  6 );
            String asinString             = record.get( 12 );
            String quantityString         = record.get( 14 );
            String offeringNameString     = record.get( 23 );
            String giftMessageString      = record.get( 24 );

            MeshObject offeringObj = ImporterUtils.findOrCreate(
                    mb.createMeshObjectIdentifierBelow(
                            "offerings",
                            asinString ),
                    AmazonSubjectArea.AMAZONPRODUCTOFFERING,
                    mb,
                    (MeshObject created) -> {
                        created.setPropertyValue( CommerceSubjectArea.OFFERING_NAME, StringValue.create( offeringNameString ));
                        created.setPropertyValue( CommerceSubjectArea.OFFERING_PRICE, parseCurrency( currencyString, unitPriceString ));
                        created.setPropertyValue( AmazonSubjectArea.AMAZONPRODUCTOFFERING_ASIN, StringValue.create( asinString ));
                    } );

            MeshObject orderObject = ImporterUtils.findOrCreate(
                    mb.createMeshObjectIdentifierBelow(
                            "order",
                            orderIdString ),
                    AmazonSubjectArea.AMAZONORDER,
                    mb,
                    (MeshObject created) -> {
                        created.setPropertyValue( CommerceSubjectArea.ORDER_ORDEREDON, parseTimeStamp( orderedOnString ));
                    } );

            MeshObject lineItemObj = ImporterUtils.createWithoutConflict(
                    mb.createMeshObjectIdentifierBelow(
                            orderObject.getIdentifier(),
                            asinString ),
                    AmazonSubjectArea.AMAZONORDERLINEITEM,
                    mb );
                    // there is no unique identifier for Purchases. And apparently multiple purchases of the same
                    // ASIN can happen in the same Order, as long as the gift card message is different
            lineItemObj.setPropertyValue( CommerceSubjectArea.ORDERLINEITEM_QUANTITY,       parseInteger( quantityString ));
            lineItemObj.setPropertyValue( CommerceSubjectArea.ORDERLINEITEM_UNITPRICE,      parseCurrency( currencyString, unitPriceString ));
            lineItemObj.setPropertyValue( CommerceSubjectArea.ORDERLINEITEM_UNITSALESTAX,   parseCurrency( currencyString, unitSalesTaxString ));

            if( !giftMessageString.isBlank() ) {
                lineItemObj.setPropertyValue(
                        AmazonSubjectArea.AMAZONORDERLINEITEM_GIFTMESSAGE,
                        AmazonSubjectArea.AMAZONORDERLINEITEM_GIFTMESSAGE_type.createBlobValue( giftMessageString, SelectableMimeType.TEXT_PLAIN.getMimeType() ));
            }
            lineItemObj.blessRole( CommerceSubjectArea.ORDERLINEITEM_REFERENCES_OFFERING_S, offeringObj );
            orderObject.blessRole( CommerceSubjectArea.ORDER_INCLUDES_ORDERLINEITEM_S, lineItemObj );
        }

        return PERFECT;
    }
}
