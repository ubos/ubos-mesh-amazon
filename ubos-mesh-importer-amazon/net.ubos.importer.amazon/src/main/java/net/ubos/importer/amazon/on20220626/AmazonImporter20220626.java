//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.amazon.on20220626;

import java.io.IOException;
import net.ubos.importer.amazon.AbstractAmazonImporter;
import net.ubos.importer.amazon.on20201031.Advertising.AdvertiserAudiencesHandler;
import net.ubos.importer.amazon.on20201031.Advertising.AdvertiserClicksHandler;
import net.ubos.importer.amazon.on20201031.Advertising.AudiencesHandler;
import net.ubos.importer.amazon.on20220626.RetailOrderHistory.OrderHistoryHandler;
import net.ubos.importer.amazon.on20201031.SearchData.CustomerEngagementHandler;
import net.ubos.importer.handler.ImporterHandler;
import net.ubos.importer.handler.csv.DefaultCsvImporterHandler;
import net.ubos.importer.handler.directory.SkipIntermediateDirectoryImporterHandler;
import net.ubos.importer.handler.file.DefaultFileImporterHandler;
import net.ubos.importer.handler.json.DefaultJsonImporterHandler;
import net.ubos.importer.handler.remainders.RateRemaindersImporterHandler;
import net.ubos.importer.handler.zip.DefaultZipImporterHandler;
import net.ubos.util.logging.Log;

/**
 * Imports a directory containing Amazon export ZIP files.
 */
public class AmazonImporter20220626
    extends
        AbstractAmazonImporter
{
    private static final Log log = Log.getLogInstance( AmazonImporter20220626.class );

    /**
     * Constructor.
     */
    public AmazonImporter20220626()
    {
        super( "Amazon importer (format as of 2022-06-26)", HANDLERS, RATE_REMAINDERS_HANDLER );
    }

    /**
     * Name of the file that contains the expected entry patterns.
     */
    public static final String EXPECTED_ENTRY_PATTERNS_FILE = "ExpectedEntryPatterns.txt";

    /**
     * The ImporterHandlers for this Importer.
     *
     * This accepts either the original ZIP files, or directories created by unpacking them with the same
     * name but no extension.
     */
    public static final ImporterHandler [] HANDLERS = {
            // Structure first
                new SkipIntermediateDirectoryImporterHandler(),
                new DefaultZipImporterHandler(),

            // Advertising

                new AudiencesHandler(           "Advertising\\.\\d(?:\\.zip)?/Advertising\\.3PAudiences\\.csv",
                        "Audiences in which you are included via 3rd Parties",
                        "3paudiences",
                        "Third-party audiences" ),
                new AdvertiserAudiencesHandler( "Advertising\\.\\d(?:\\.zip)?/Advertising\\.AdvertiserAudiences\\.csv" ),
                new AdvertiserClicksHandler(    "Advertising\\.\\d(?:\\.zip)?/Advertising\\.AdvertiserClicks\\.csv" ),
                new AudiencesHandler(           "Advertising\\.\\d(?:\\.zip)?/Advertising\\.AmazonAudiences\\.csv",
                        "Amazon Audiences in which you are included",
                        "amazonaudiences",
                        "Amazon audiences" ),

            // RetailOrderHistory

                new OrderHistoryHandler(
                        "Retail\\.OrderHistory\\.\\d+(?:\\.zip)?/Retail\\.OrderHistory\\.\\d+\\.csv" ),
                // new OrdersReturnedHandler(
                //         "Retail\\.OrdersReturned(?:\\.zip)?/Retail\\.OrdersReturned\\.csv" ),

            // SearchData
                new CustomerEngagementHandler(  "Search-Data(?:\\.zip)?/Search-Data\\.Customer-Engagement\\.csv" ),

            // Fallback

                new DefaultCsvImporterHandler.Builder().minColumns( 1 ).build(),
                new DefaultJsonImporterHandler( ImporterHandler.FALLBACK ),
                new DefaultFileImporterHandler( ImporterHandler.UNRATED ), // includes .eml files

            };

    public static final RateRemaindersImporterHandler RATE_REMAINDERS_HANDLER;
    static {
        RateRemaindersImporterHandler handler = null;
        try {
            handler = new RateRemaindersImporterHandler(
                        AmazonImporter20220626.class.getResourceAsStream( EXPECTED_ENTRY_PATTERNS_FILE ),
                        ImporterHandler.SUFFICIENT, // we aren't actually parsing, but that's intentionally so
                        ImporterHandler.MAYBE_WRONG );

        } catch( IOException ex ) {
            log.error( ex );
        }
        RATE_REMAINDERS_HANDLER = handler;
    }
}
