//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.amazon.on20201031.Advertising;

import java.io.IOException;
import java.text.ParseException;
import java.util.Iterator;
import java.util.regex.Pattern;
import net.ubos.importer.ImporterUtils;
import net.ubos.importer.amazon.AbstractAmazonCsvImportContentHandler;
import net.ubos.importer.amazon.AmazonImporterHandlerContext;
import net.ubos.importer.handler.ImporterHandlerContext;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.MeshBase;
import net.ubos.model.Amazon.AmazonSubjectArea;
import net.ubos.model.Marketing.MarketingSubjectArea;
import net.ubos.model.primitives.StringValue;
import net.ubos.util.ArrayHelper;
import net.ubos.util.logging.Log;
import org.apache.commons.csv.CSVRecord;

/**
 * Knows how to import audiences.
 */
public class AudiencesHandler
    extends
        AbstractAmazonCsvImportContentHandler
{
    private static final Log log = Log.getLogInstance(AudiencesHandler.class );

    /**
     * Constructor.
     */
    public AudiencesHandler(
            String insideZipFilePattern,
            String expectedHeaderLabel,
            String collectionIdPrefix,
            String collectionName )
    {
        super( insideZipFilePattern,
               new String[] { expectedHeaderLabel });

        theCollectionIdPrefix = collectionIdPrefix;
        theCollectionName     = collectionName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double doImport(
            AmazonImporterHandlerContext context,
            Iterator<CSVRecord>          recordIter )
        throws
            ParseException,
            IOException
    {
        MeshBase mb = context.getMeshBase();

        while( recordIter.hasNext() ) {
            CSVRecord record = recordIter.next();

            String    topicString     = record.get( 0 );
            String [] topicsHierarchy = topicString.split( ":" );

            MeshObject lastInterest = null;
            for( int i=0 ; i<topicsHierarchy.length ; ++i ) {
                String identifier = topicsHierarchy[i].replaceAll( "/", " " );

                MeshObjectIdentifier parentInterestId;
                if( lastInterest == null ) {
                    parentInterestId = mb.createMeshObjectIdentifier( "audience" );
                } else {
                    parentInterestId = lastInterest.getIdentifier();
                }
                String interestName = String.join( ":" , ArrayHelper.copyIntoNewArray( topicsHierarchy, 0, i+1, String.class ));

                MeshObject interest = ImporterUtils.findOrCreate(
                        mb.createMeshObjectIdentifierBelow(
                                parentInterestId,
                                identifier ),
                        AmazonSubjectArea.AMAZONAUDIENCE,
                        mb,
                        (MeshObject created) -> {
                            created.setPropertyValue(
                                    MarketingSubjectArea.INTEREST_NAME,
                                    StringValue.create( interestName ));

                            try {
                                MeshObject interestCollection = ImporterUtils.findOrCreate(
                                    theCollectionIdPrefix,
                                    AmazonSubjectArea.AMAZONAUDIENCECOLLECTION,
                                    mb,
                                    (MeshObject created2) -> {
                                        created2.setPropertyValue( AmazonSubjectArea.AMAZONAUDIENCECOLLECTION_NAME, StringValue.create( theCollectionName ));
                                        context.getAccount().blessRole( MarketingSubjectArea.ACCOUNT_HASASSOCIATED_MARKETINGINFO_S, created2 );
                                    } );

                                interestCollection.blessRole( MarketingSubjectArea.INTERESTCOLLECTION_COLLECTS_INTEREST_S, created );
                            } catch( ParseException ex ) {
                                log.error( ex );
                            }
                        } );

                if( lastInterest != null && !interest.isRelated( AmazonSubjectArea.AMAZONAUDIENCE_ISSPECIALIZATIONOF_AMAZONAUDIENCE_S, lastInterest )) {
                    interest.blessRole( AmazonSubjectArea.AMAZONAUDIENCE_ISSPECIALIZATIONOF_AMAZONAUDIENCE_S, lastInterest );
                }

                lastInterest = interest;
            }
        }
        return PERFECT;
    }

    protected final String theCollectionIdPrefix;
    protected final String theCollectionName;
}
