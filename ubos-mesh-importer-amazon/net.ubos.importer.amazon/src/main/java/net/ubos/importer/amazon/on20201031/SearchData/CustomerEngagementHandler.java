//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.amazon.on20201031.SearchData;

import java.io.IOException;
import java.text.ParseException;
import java.util.Iterator;
import java.util.Map;
import net.ubos.importer.ImporterUtils;
import net.ubos.importer.amazon.AbstractAmazonCsvImportContentHandler;
import net.ubos.importer.amazon.AmazonImporterHandlerContext;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.MeshBase;
import net.ubos.model.Amazon.AmazonSubjectArea;
import net.ubos.model.Search.SearchSubjectArea;
import net.ubos.model.primitives.EnumeratedValue;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.TimeStampValue;
import org.apache.commons.csv.CSVRecord;

/**
 *
 */
public class CustomerEngagementHandler
    extends
        AbstractAmazonCsvImportContentHandler
{
    public CustomerEngagementHandler(
            String insideZipFilePattern )
    {
        super(  insideZipFilePattern,
                new String[] {
                    /*  0 */ "First Search Time (GMT)",                 // When
                    /*  1 */ "All Department (APS) or Category",        // Category
                    /*  2 */ "Site Variant",
                    /*  3 */ "Application / Browser Name",
                    /*  4 */ "Device Model",
                    /*  5 */ "Search Type (Keyword,Visual,Browse)",     // SearchType
                    /*  6 */ "Session ID",
                    /*  7 */ "Query ID",
                    /*  8 */ "Prime Customer (Y/N)",
                    /*  9 */ "Is From External Link (Y/N)",
                    /* 10 */ "Search From External Site (Y/N)",
                    /* 11 */ "First Search Query String",
                    /* 12 */ "Application Name",
                    /* 13 */ "App Version",
                    /* 14 */ "Operating System Name",
                    /* 15 */ "Operating System Version",
                    /* 16 */ "Device Type ID",
                    /* 17 */ "Device Category",
                    /* 18 */ "Customer IP",
                    /* 19 */ "Search Method",
                    /* 20 */ "Keywords",                                // SearchQuery
                    /* 21 */ "Amazon Business Customer (Y/N)",
                    /* 22 */ "Language",
                    /* 23 */ "Server",
                    /* 24 */ "Amazon Fresh Customer (Y/N)",
                    /* 25 */ "Music Subscriber (Y/N)",
                    /* 26 */ "First Browse Node",
                    /* 27 */ "Last search Time (GMT)",
                    /* 28 */ "Last Department",
                    /* 29 */ "Last Browse Node",
                    /* 30 */ "First Added Item",
                    /* 31 */ "First Purchased Item",
                    /* 32 */ "First Consumed Item (Subscription)",
                    /* 33 */ "Number of Clicked Items",
                    /* 34 */ "Number of Items Added to Cart",
                    /* 35 */ "Number of Items Ordered",
                    /* 36 */ "Number of Paid Items Ordered",
                    /* 37 */ "Number of Free Items Ordered",
                    /* 38 */ "Units Ordered",
                    /* 39 */ "Paid Units Ordered",
                    /* 40 */ "Free Units Ordered",
                    /* 41 */ "Maximum Purchase Price",
                    /* 42 */ "Clicked Any Item (Y/N)",
                    /* 43 */ "Added Any Item (Y/N)",
                    /* 44 */ "Purchased Any Item (Y/N)",
                    /* 45 */ "Department Count",
                    /* 46 */ "Shopping Refinement",
                    /* 47 */ "Number of Shopping Refinements",
                    /* 48 */ "Highest Number of Shopping Refinements",
                    /* 49 */ "Items Consumed (Subscription)",
                    /* 50 */ "Shopping Refinement Pickers",
                    /* 51 */ "Paid Purchase (Y/N)",
                    /* 52 */ "Item Borrowed (Y/N)",
                    /* 53 */ "Items Borrowed",
                    /* 54 */ "Next Query Group via Click",
                    /* 55 */ "Query Abandoned (Y/N)",
                    /* 56 */ "Query Reformulated (Y/N)",
                    /* 57 */ "Amazon Fresh (Y/N)",
                    /* 58 */ "Store Visited",
                    /* 59 */ "Department",
                    /* 60 */ "Browse Node",
                    /* 61 */ "First Search Domain",
                    /* 62 */ "Is First Search From External Ad",
                    /* 63 */ "User Agent Info Family"
                } );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double doImport(
            AmazonImporterHandlerContext context,
            Iterator<CSVRecord>          recordIter )
        throws
            ParseException,
            IOException
    {
        MeshBase mb = context.getMeshBase();

        while( recordIter.hasNext() ) {
            CSVRecord record = recordIter.next();

            String whenString        = record.get(  0 );
            String categoryString    = record.get(  1 );
            String searchTypeString  = record.get(  5 );
            String searchQueryString = record.get( 20 );

            TimeStampValue when = parseTimeStamp( whenString );

            MeshObjectIdentifier baseId = mb.createMeshObjectIdentifierBelow(
                    "searches",
                    String.valueOf( when.getAsMillis() ));
            MeshObjectIdentifier candidateId = baseId;
            int count = 0;

            while( mb.findMeshObjectByIdentifier( candidateId ) != null ) {
                candidateId = mb.createMeshObjectIdentifierBelow( baseId, String.valueOf( ++count ));
            }

            MeshObject searchQuery = ImporterUtils.createWithoutConflict(
                    mb.createMeshObjectIdentifierBelow( "searches", when.getValue() ),
                    AmazonSubjectArea.AMAZONSEARCHQUERY,
                    mb );

            searchQuery.setPropertyValue( SearchSubjectArea.SEARCHQUERY_QUERY, StringValue.create( searchQueryString ));
            searchQuery.setPropertyValue( SearchSubjectArea.SEARCHQUERY_PERFORMEDWHEN, when );
            searchQuery.setPropertyValue( AmazonSubjectArea.AMAZONSEARCHQUERY_CATEGORY, StringValue.create( categoryString ) );
            searchQuery.setPropertyValue( AmazonSubjectArea.AMAZONSEARCHQUERY_SEARCHTYPE, theSearchTypes.get( searchTypeString.toLowerCase() ));

            context.getAccount().blessRole( SearchSubjectArea.ACCOUNT_PERFORMS_SEARCHQUERY_S, searchQuery );
        }
        return PERFECT;
    }

    /**
     * Maps search type strings into the appropriate EnumeratedValues.
     */
    protected static final Map<String,EnumeratedValue> theSearchTypes = Map.of(
            "kw", AmazonSubjectArea.AMAZONSEARCHQUERY_SEARCHTYPE_type_KEYWORD
    );
}