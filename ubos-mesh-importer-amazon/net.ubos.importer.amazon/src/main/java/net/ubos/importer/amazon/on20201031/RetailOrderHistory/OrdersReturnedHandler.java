//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.amazon.on20201031.RetailOrderHistory;

import java.io.IOException;
import java.text.ParseException;
import java.util.Iterator;
import java.util.regex.Pattern;
import net.ubos.importer.amazon.AbstractAmazonCsvImportContentHandler;
import net.ubos.importer.amazon.AmazonImporterHandlerContext;
import net.ubos.importer.handler.ImporterHandlerContext;
import org.apache.commons.csv.CSVRecord;

/**
 *
 */
public class OrdersReturnedHandler
    extends
        AbstractAmazonCsvImportContentHandler
{
    public OrdersReturnedHandler(
            String insideZipFilePattern )
    {
        super(  insideZipFilePattern,
                new String[] {
                    "Marketplace",
                    "ReversalID",
                    "OrderID",
                    "CreationDate",
                    "Quantity",
                    "Currency",
                    "ReversalStatus",
                    "ReversalAmountState",
                    "ReversalReason",
                    "DirectDebitRefund"
                });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double doImport(
            AmazonImporterHandlerContext context,
            Iterator<CSVRecord>          recordIter )
        throws
            ParseException,
            IOException
    {
        return FALLBACK; // FIXME
    }
}