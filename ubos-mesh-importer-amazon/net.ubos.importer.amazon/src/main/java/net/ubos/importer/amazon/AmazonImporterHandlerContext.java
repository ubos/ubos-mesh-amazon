//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.amazon;

import net.ubos.importer.handler.ImporterHandlerContext;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.history.EditableHistoryMeshBase;

/**
 * Addition data for the Amazon importers.
 */
public class AmazonImporterHandlerContext
    extends
        ImporterHandlerContext
{
    /**
     * Constructor.
     *
     * @param accountMeshObject the MeshObject that represents the Amazon account
     * @param timeOfExport the time when the data source being imported was exported
     * @param mb the MeshBase
     */
    public AmazonImporterHandlerContext(
            MeshObject              accountMeshObject,
            long                    timeOfExport,
            EditableHistoryMeshBase mb )
    {
        super( timeOfExport, mb );

        theAccountMeshObject = accountMeshObject;
    }

    /**
     * Obtain the Amazon account MeshObject
     *
     * @return
     */
    public MeshObject getAccount()
    {
        return theAccountMeshObject;
    }

    /**
     * The MeshObject representing the Amazon account.
     */
    protected final MeshObject theAccountMeshObject;
}
