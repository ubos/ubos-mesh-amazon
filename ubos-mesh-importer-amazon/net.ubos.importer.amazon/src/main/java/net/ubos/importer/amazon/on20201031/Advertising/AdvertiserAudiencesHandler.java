//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.amazon.on20201031.Advertising;

import java.io.IOException;
import java.text.ParseException;
import java.util.Iterator;
import net.ubos.importer.ImporterUtils;
import net.ubos.importer.amazon.AbstractAmazonCsvImportContentHandler;
import net.ubos.importer.amazon.AmazonImporterHandlerContext;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBase;
import net.ubos.model.Amazon.AmazonSubjectArea;
import net.ubos.model.Marketing.MarketingSubjectArea;
import net.ubos.model.primitives.StringValue;
import org.apache.commons.csv.CSVRecord;

/**
 * Knows how to import advertiser audiences.
 */
public class AdvertiserAudiencesHandler
    extends
        AbstractAmazonCsvImportContentHandler
{
    public AdvertiserAudiencesHandler(
            String insideZipFilePattern )
    {
        super( insideZipFilePattern,
               new String[] { "Advertisers who brought audiences in which you are included" } );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double doImport(
            AmazonImporterHandlerContext context,
            Iterator<CSVRecord>          recordIter )
        throws
            ParseException,
            IOException
    {
        MeshBase mb = context.getMeshBase();

        while( recordIter.hasNext() ) {
            CSVRecord record = recordIter.next();

            String advertiser = record.get( 0 );

            MeshObject advertiserObj = ImporterUtils.findOrCreate(
                    mb.createMeshObjectIdentifierBelow(
                            "advertisers",
                            advertiser.replaceAll( "/", "" ).replaceAll( " +", " " ) ),
                    AmazonSubjectArea.AMAZONADVERTISER,
                    mb,
                    (MeshObject created) -> {
                        created.setPropertyValue( MarketingSubjectArea.ADVERTISER_NAME, StringValue.create( advertiser ));

                        created.blessRole( MarketingSubjectArea.ADVERTISER_BRINGSAUDIENCEINCLUDING_ACCOUNT_S, context.getAccount());
                    } );
        }
        return PERFECT;
    }
}
