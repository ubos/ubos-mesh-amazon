//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.amazon;

import net.ubos.importer.Importer;
import net.ubos.importer.amazon.on20201031.AmazonImporter20201031;
import net.ubos.importer.amazon.on20220626.AmazonImporter20220626;
import net.ubos.importer.amazon.on20220817.AmazonImporter20220817;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleActivationException;

/**
 * Activate this Module and return an Importer
 */
public class ModuleInit
{
    /**
     * Diet4j module activation.
     *
     * @param thisModule the Module being activated
     * @return the Importer instances
     * @throws ModuleActivationException thrown if module activation failed
     */
    public static Importer [] moduleActivate(
            Module thisModule )
        throws
            ModuleActivationException
    {
        return new Importer[] {
            new AmazonImporter20201031(),
            new AmazonImporter20220626(),
            new AmazonImporter20220817()
        };
    }
}
