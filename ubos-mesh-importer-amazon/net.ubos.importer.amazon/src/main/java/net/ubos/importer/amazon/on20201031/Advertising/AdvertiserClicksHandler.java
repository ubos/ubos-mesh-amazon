//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.amazon.on20201031.Advertising;

import java.io.IOException;
import java.text.ParseException;
import java.util.Iterator;
import net.ubos.importer.ImporterUtils;
import net.ubos.importer.amazon.AbstractAmazonCsvImportContentHandler;
import net.ubos.importer.amazon.AmazonImporterHandlerContext;
import net.ubos.importer.handler.ImporterHandlerContext;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBase;
import net.ubos.model.Amazon.AmazonSubjectArea;
import net.ubos.model.Marketing.MarketingSubjectArea;
import net.ubos.model.primitives.StringValue;
import org.apache.commons.csv.CSVRecord;

/**
 *
 */
public class AdvertiserClicksHandler
    extends
        AbstractAmazonCsvImportContentHandler
{
    public AdvertiserClicksHandler(
            String insideZipFilePattern )
    {
        super( insideZipFilePattern,
               new String[] { "Advertisers whose ads you clicked" } );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double doImport(
            AmazonImporterHandlerContext context,
            Iterator<CSVRecord>          recordIter )
        throws
            ParseException,
            IOException
    {
        MeshBase mb = context.getMeshBase();

        while( recordIter.hasNext() ) {
            CSVRecord record = recordIter.next();

            String name = record.get( 0 );

            MeshObject advertiserObj = ImporterUtils.findOrCreate(
                    mb.createMeshObjectIdentifierBelow(
                            "advertisers",
                            name ),
                    AmazonSubjectArea.AMAZONADVERTISER,
                    mb,
                    (MeshObject created) -> {
                        created.setPropertyValue( MarketingSubjectArea.ADVERTISER_NAME, StringValue.create( name ));
                        created.blessRole( MarketingSubjectArea.ADVERTISER_BRINGSAUDIENCEINCLUDING_ACCOUNT_S, context.getAccount() );
                    } );

            MeshObject interactionObj = ImporterUtils.findOrCreate(
                    mb.createMeshObjectIdentifierBelow(
                            "advertisers",
                            name,
                            "interaction" ),
                    AmazonSubjectArea.AMAZONADVERTISEMENTINTERACTION,
                    mb,
                    (MeshObject created) -> {
                        created.setPropertyValue( MarketingSubjectArea.ADVERTISEMENTINTERACTION_TYPE, MarketingSubjectArea.ADVERTISEMENTINTERACTION_TYPE_type_CLICK );
                        created.blessRole( MarketingSubjectArea.ADVERTISEMENTINTERACTION_WITHADVERTISEMENTBY_ADVERTISER_S, advertiserObj );
                        context.getAccount().blessRole( MarketingSubjectArea.ACCOUNT_PERFORMS_ADVERTISEMENTINTERACTION_S, created );
                    } );
        }

        return PERFECT;
    }
}
