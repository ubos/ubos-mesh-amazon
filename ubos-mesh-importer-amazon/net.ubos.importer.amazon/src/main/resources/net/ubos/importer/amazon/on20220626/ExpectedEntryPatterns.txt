[^/]+
[^/]+/Advertising\.\d+\.zip
[^/]+/Advertising\.\d+\.zip/Advertising\.3PAudiences\.csv
[^/]+/Advertising\.\d+\.zip/Advertising\.AdvertiserAudiences\.csv
[^/]+/Advertising\.\d+\.zip/Advertising\.AdvertiserClicks\.csv
[^/]+/Advertising\.\d+\.zip/Advertising\.AmazonAudiences\.csv
[^/]+/Advertising\.\d+\.zip/Advertising\.OptOut\.csv
[^/]+/Alexa\.zip
[^/]+/Alexa\.zip/Alerts
[^/]+/Alexa\.zip/Alerts/Mobile_Push_Notification_Permission-\d+\.csv
[^/]+/Alexa\.zip/Alexa/Devices/Push Notifications/Push Notifications\.csv
[^/]+/Alexa\.zip/AlexaProactive/ProactiveRecommendations-\d+\.csv
[^/]+/Alexa\.zip/Answers
[^/]+/Alexa\.zip/Answers/AnswerGroups/User Stats.csv
[^/]+/Alexa\.zip/Calendars
[^/]+/Alexa\.zip/Calendars/Calendars-\d+\.csv
[^/]+/Alexa\.zip/Devices
[^/]+/Alexa\.zip/Devices/Mobile/\d+\.csv
[^/]+/Alexa\.zip/Devices/Touch Events\.csv
[^/]+/Alexa\.zip/Feedback
[^/]+/Alexa\.zip/Feedback/Feedback-\d+\.csv
[^/]+/Alexa\.zip/Lists
[^/]+/Alexa\.zip/Lists/Lists-\d+\.csv
[^/]+/Alexa\.zip/Location
[^/]+/Alexa\.zip/Location/Country of Residence-\d+\.csv
[^/]+/Alexa\.zip/Preferences
[^/]+/Alexa\.zip/Preferences/Preferences\.json
[^/]+/Alexa\.zip/Settings
[^/]+/Alexa\.zip/Settings/Settings
[^/]+/Alexa\.zip/Settings/Settings/User settings
[^/]+/Alexa\.zip/Settings/Settings/User settings/Settings-\d+\.csv
[^/]+/Alexa\.zip/Settings/VoiceProfileStatus
[^/]+/Alexa\.zip/Settings/VoiceProfileStatus/VoiceProfileStatus-\d+\.csv
[^/]+/Alexa\.zip/Shopping
[^/]+/Alexa\.zip/Shopping/Sharable list\.csv
[^/]+/Alexa\.zip/Shopping/Voice Purchase Settings\.csv
[^/]+/Alexa\.zip/Skills
[^/]+/Alexa\.zip/Skills/Karaoke
[^/]+/Alexa\.zip/Skills/Karaoke/Karaoke\.csv
[^/]+/Alexa\.zip/Smart Home
[^/]+/Alexa\.zip/Smart Home/Camera Smart Alerts
[^/]+/Alexa\.zip/Smart Home/Camera Smart Alerts/Camera Smart Alerts\.csv
[^/]+/Alexa\.zip/Smart Home/Cameras Smart Alerts
[^/]+/Alexa\.zip/Smart Home/Cameras Smart Alerts/Camera Smart Alerts\.csv
[^/]+/Alexa\.zip/Smart Home/Device Configuration History\.csv
[^/]+/Alexa\.zip/Smart Home/Device Configuration-\d+\.csv
[^/]+/Alexa\.zip/Smart Home/Device State-\d+\.csv
[^/]+/Alexa\.zip/Smart Home/Devices Data\.csv
[^/]+/Alexa\.zip/Smart Home/Hunches\.csv
[^/]+/Amazon-Drive\.zip
[^/]+/Amazon-Drive\.zip/account_info_\d+\.csv
[^/]+/Amazon-Drive\.zip/node_metadata_na_\d+\.csv
[^/]+/Amazon-Drive\.zip/Readme\.txt
[^/]+/Amazon-Music\.zip
[^/]+/Amazon-Music\.zip/account\.csv
[^/]+/Amazon-Music\.zip/accountPreferences\.csv
[^/]+/Amazon-Music\.zip/profile\.csv
[^/]+/Amazon\.Lists\.Wishlist\.\d+\.\d+\.zip
[^/]+/Amazon\.Lists\.Wishlist\.\d+\.\d+\.zip/Amazon\.Lists\.Wishlist\.json
[^/]+/AmazonGames\.zip
[^/]+/AmazonGames\.zip/LostArk_Forums_Instructions\.txt
[^/]+/AmazonGames\.zip/NewWorld_Forums_Instructions\.txt
[^/]+/Appstore\.Entitlements\.zip
[^/]+/Appstore\.Entitlements\.zip/Appstore.Entitlements/datasets
[^/]+/Appstore\.Entitlements\.zip/Appstore.Entitlements/datasets/PDI
[^/]+/Appstore\.Entitlements\.zip/Appstore.Entitlements/datasets/PDI/PDI\.csv
[^/]+/Appstore\.zip
[^/]+/Appstore\.zip/Appstore
[^/]+/Appstore\.zip/Appstore/datasets
[^/]+/Appstore\.zip/Appstore/datasets/subscription-period
[^/]+/Appstore\.zip/Appstore/datasets/subscription-period/subscription-period\.csv
[^/]+/Appstore\.zip/Appstore/datasets/subscription-transaction
[^/]+/Appstore\.zip/Appstore/datasets/subscription-transaction/subscription-transaction\.csv
[^/]+/Audible\.AdobeAnalytics
[^/]+/Audible\.AdobeAnalytics/Audible.AdobeAnalytics\.csv
[^/]+/Audible\.CartHistory\.zip
[^/]+/Audible\.CartHistory\.zip/Audible\.CartHistory.csv
[^/]+/Audible\.DevicesActivations\.zip
[^/]+/Audible\.DevicesActivations\.zip/Audible\.DevicesActivations\.csv
[^/]+/Audible\.Library\.zip
[^/]+/Audible\.Library\.zip/Audible\.Library\.csv
[^/]+/Audible\.Listening\.zip
[^/]+/Audible\.Listening\.zip/Audible\.Listening\.csv
[^/]+/Audible\.PurchaseHistory\.zip
[^/]+/Audible\.PurchaseHistory\.zip/Audible\.PurchaseHistory\.csv
[^/]+/CustomerCommunicationExperience\.Preferences\.zip
[^/]+/CustomerCommunicationExperience\.Preferences\.zip/CustomerCommunicationExperience\.Preferences
[^/]+/CustomerCommunicationExperience\.Preferences\.zip/CustomerCommunicationExperience\.Preferences/datasets
[^/]+/CustomerCommunicationExperience\.Preferences\.zip/CustomerCommunicationExperience\.Preferences/datasets/CustomerCommunicationExperience\.Preferences
[^/]+/CustomerCommunicationExperience\.Preferences\.zip/CustomerCommunicationExperience\.Preferences/datasets/CustomerCommunicationExperience\.Preferences/CustomerCommunicationExperience\.Preferences\.csv
[^/]+/CustomerCommunicationExperience\.Preferences\.zip/CustomerCommunicationExperience\.Preferences/datasets/CustomerCommunicationExperience\.PreferencesEmailHistory
[^/]+/CustomerCommunicationExperience\.Preferences\.zip/CustomerCommunicationExperience\.Preferences/datasets/CustomerCommunicationExperience\.PreferencesEmailHistory/CustomerCommunicationExperience\.PreferencesEmailHistory\.csv
[^/]+/Dash\.zip
[^/]+/Dash\.zip/DashDeviceRegistrations\.csv
[^/]+/Dash\.zip/DashDeviceTemplates\.csv
[^/]+/Dash\.zip/DashDeviceTemplateSlots\.csv
[^/]+/Devices\.Kindle\.UnlimitedMembership\.\d+\.zip
[^/]+/Devices\.Kindle\.UnlimitedMembership\.\d+\.zip/Devices\.Kindle\.UnlimitedMembership\.txt
[^/]+/Devices\.Kindle\.UnlimitedMembership\.\d+/Kindle Unlimited - Store Offer Data\.txt
[^/]+/Digital-Ordering\.d+\.zip
[^/]+/Digital-Ordering\.d+\.zip/Digital Items\.csv
[^/]+/Digital-Ordering\.d+\.zip/Digital Orders Monetary\.csv
[^/]+/Digital-Ordering\.d+\.zip/Digital Orders\.csv
[^/]+/Digital-Ordering\.d+\.zip/Legacy Payment Information\.csv
[^/]+/Digital-Ordering\.d+\.zip/README\.txt
[^/]+/Digital\.ActionBenefit\.\d+\.zip
[^/]+/Digital\.ActionBenefit\.\d+\.zip/Digital\.ActionBenefit\.\d+\.csv
[^/]+/Digital\.Borrows\.\d+\.zip
[^/]+/Digital\.Borrows\.\d+\.zip/Digital\.Borrows\.\d+\.csv
[^/]+/Digital\.Content\.Ownership\.zip
[^/]+/Digital\.Content\.Ownership\.zip/[^/]+\.json
[^/]+/Digital\.Content\.Whispersync\.zip
[^/]+/Digital\.Content\.Whispersync\.zip/whispersync\.csv
[^/]+/Digital\.CustomerAttributes\.\d+\.zip
[^/]+/Digital\.CustomerAttributes\.\d+\.zip/Digital\.CustomerAttributes\.\d+\.csv
[^/]+/Digital\.Orders\.Returns\.\d+\.zip
[^/]+/Digital\.Orders\.Returns\.\d+\.zip/Digital\.Orders\.Returns\.Monetary\.\d+\.csv
[^/]+/Digital\.Orders\.Returns\.\d+\.zip/Digital\.Orders\.Returns\.Transaction\.\d+\.csv
[^/]+/Digital\.Orders\.Returns\.\d+\.zip/README\.txt
[^/]+/Digital\.PrimeVideo\.CustomerTitleRelevanceRecommendations\.zip
[^/]+/Digital\.PrimeVideo\.CustomerTitleRelevanceRecommendations\.zip/Digital\.PrimeVideo\.CustomerTitleRelevanceRecommendations\.csv
[^/]+/Digital\.PrimeVideo\.CustomerTitleRelevanceRecommendations\.zip/Digital\.PrimeVideo\.CustomerTitleRelevanceRecommendations\.Description\.pdf
[^/]+/Digital\.PrimeVideo\.ViewCounts\.\d+\.zip
[^/]+/Digital\.PrimeVideo\.ViewCounts\.\d+\.zip/Digital\.PrimeVideo\.ViewCounts\.\d+\.csv
[^/]+/Digital\.PrimeVideo\.ViewCounts\.\d+\.zip/Digital\.PrimeVideo\.ViewCounts description\.pdf
[^/]+/Digital\.PrimeVideo\.Viewinghistory\.zip
[^/]+/Digital\.PrimeVideo\.Viewinghistory\.zip/Digital\.PrimeVideo\.Viewinghistory\.csv
[^/]+/Digital\.PrimeVideo\.Viewinghistory\.zip/Digital\.PrimeVideo\.Viewinghistory\.Description\.pdf
[^/]+/Digital\.PrimeVideo\.Viewinghistory\.zip/README\.txt
[^/]+/Digital\.PrimeVideo\.Watchlist\.zip
[^/]+/Digital\.PrimeVideo\.Watchlist\.zip/Digital\.PrimeVideo\.Watchlist
[^/]+/Digital\.PrimeVideo\.Watchlist\.zip/Digital\.PrimeVideo\.Watchlist/Digital\.PrimeVideo\.WatchlistHistory\.csv
[^/]+/Digital\.PrimeVideo\.Watchlist\.zip/Digital\.PrimeVideo\.Watchlist/Digital\.PrimeVideo\.Watchlist-WatchlistHistory-Description\.pdf
[^/]+/Digital\.Redemption\.\d+\.zip
[^/]+/Digital\.Redemption\.\d+\.zip/Digital\.Redemption\.\d+\.csv
[^/]+/Digital\.Redemption\.\d+\.zip/Digital\.Redemption\.Source\.\d+\.csv
[^/]+/Digital\.SeriesContent\.Relation_\d+\.zip
[^/]+/Digital\.SeriesContent\.Relation_\d+\.zip/BookRelation\.csv
[^/]+/Digital\.SeriesContent\.Relation_\d+\.zip/SeriesRelation\.csv
[^/]+/Digital\.Subscriptions
[^/]+/Digital\.Subscriptions/Beneficiaries\.csv
[^/]+/Digital\.Subscriptions/Billing Schedule Items\.csv
[^/]+/Digital\.Subscriptions/Billing Schedules\.csv
[^/]+/Digital\.Subscriptions/Payment Plan Items\.csv
[^/]+/Digital\.Subscriptions/Payment Plans\.csv
[^/]+/Digital\.Subscriptions/README\.txt
[^/]+/Digital\.Subscriptions/Subscription Periods\.csv
[^/]+/Digital\.Subscriptions/Subscription Problems\.csv
[^/]+/Digital\.Subscriptions/Subscription Status History\.csv
[^/]+/Digital\.Subscriptions/Subscriptions\.csv
[^/]+/HomeServices\.HomeInnovationTechnology\.Discover\.\d+\.zip
[^/]+/HomeServices\.HomeInnovationTechnology\.Discover\.\d+\.zip/likes
[^/]+/HomeServices\.HomeInnovationTechnology\.Discover\.\d+\.zip/likes/MyLikes\.csv
[^/]+/HomeServices\.HomeInnovationTechnology\.Pets\.\d+
[^/]+/HomeServices\.HomeInnovationTechnology\.Pets\.\d+/pets
[^/]+/HomeServices\.HomeInnovationTechnology\.Pets\.\d+/pets/pets\.csv
[^/]+/Kindle\.AuthorFollows\.zip
[^/]+/Kindle\.AuthorFollows\.zip/Kindle\.AuthorFollows/datasets
[^/]+/Kindle\.AuthorFollows\.zip/Kindle\.AuthorFollows/datasets/Kindle\.AuthorFollows\.AuthorRecommendations
[^/]+/Kindle\.AuthorFollows\.zip/Kindle\.AuthorFollows/datasets/Kindle\.AuthorFollows\.AuthorRecommendations/Kindle\.AuthorFollows\.AuthorRecommendations\.csv
[^/]+/Kindle\.BooksPromotions\.zip
[^/]+/Kindle\.BooksPromotions\.zip/Kindle.BooksPromotions/datasets
[^/]+/Kindle\.BooksPromotions\.zip/Kindle.BooksPromotions/datasets/Kindle\.BooksPromotions\.RewardOfferRepository
[^/]+/Kindle\.BooksPromotions\.zip/Kindle.BooksPromotions/datasets/Kindle\.BooksPromotions\.RewardOfferRepository/Kindle\.BooksPromotions\.RewardOfferRepository\.csv
[^/]+/Kindle\.BooksSettlers\.zip
[^/]+/Kindle\.BooksSettlers\.zip/Kindle\.BooksSettlers
[^/]+/Kindle\.BooksSettlers\.zip/Kindle\.BooksSettlers/datasets
[^/]+/Kindle\.BooksSettlers\.zip/Kindle\.BooksSettlers/datasets/Kindle\.BooksSettlers\.CustomerSignals
[^/]+/Kindle\.BooksSettlers\.zip/Kindle\.BooksSettlers/datasets/Kindle\.BooksSettlers\.CustomerSignals/Kindle\.BooksSettlers\.CustomerSignals\.csv
[^/]+/Kindle\.Devices\.ApplicationSession\.zip
[^/]+/Kindle\.Devices\.ApplicationSession\.zip/Kindle\.Devices\.ApplicationSession\.csv
[^/]+/Kindle\.Devices\.InbookToolbarInteractions\.zip
[^/]+/Kindle\.Devices\.InbookToolbarInteractions\.zip/Kindle\.Devices\.InbookToolbarInteractions\.csv
[^/]+/Kindle\.Devices\.KindleAaSettingsContinuousScroll\.zip
[^/]+/Kindle\.Devices\.KindleAaSettingsContinuousScroll\.zip/Kindle\.Devices\.KindleAaSettingsContinuousScroll\.csv
[^/]+/Kindle\.Devices\.KindleAaSettingsNumSideloadedFonts\.zip
[^/]+/Kindle\.Devices\.KindleAaSettingsNumSideloadedFonts\.zip/Kindle\.Devices\.KindleAaSettingsNumSideloadedFonts\.csv
[^/]+/Kindle\.Devices\.KindleAaSettingsReadingProgressMode\.zip
[^/]+/Kindle\.Devices\.KindleAaSettingsReadingProgressMode\.zip/Kindle\.Devices\.KindleAaSettingsReadingProgressMode\.csv
[^/]+/Kindle\.Devices\.KindleNotificationsEventsIos\.zip
[^/]+/Kindle\.Devices\.KindleNotificationsEventsIos\.zip/Kindle\.Devices\.KindleNotificationsEventsIos\.csv
[^/]+/Kindle\.Devices\.LookupCardActions\.zip
[^/]+/Kindle\.Devices\.LookupCardActions\.zip/Kindle\.Devices\.LookupCardActions\.csv
[^/]+/Kindle\.Devices\.ReaderInBookNavigation\.zip
[^/]+/Kindle\.Devices\.ReaderInBookNavigation\.zip/Kindle\.Devices\.ReaderInBookNavigation\.csv
[^/]+/Kindle\.Devices\.ReadingActionsContainers\.zip
[^/]+/Kindle\.Devices\.ReadingActionsContainers\.zip/Kindle\.Devices\.ReadingActionsContainers\.csv
[^/]+/Kindle\.Devices\.ReadingActionsWidgets\.zip
[^/]+/Kindle\.Devices\.ReadingActionsWidgets\.zip/Kindle\.Devices\.ReadingActionsWidgets\.csv
[^/]+/Kindle\.Devices\.ReadingSession\.zip
[^/]+/Kindle\.Devices\.ReadingSession\.zip/Kindle\.Devices\.ReadingSession\.csv
[^/]+/Kindle\.Devices\.ReadingSessionVersion2\.zip
[^/]+/Kindle\.Devices\.ReadingSessionVersion2\.zip/Kindle\.Devices\.ReadingSessionVersion2\.csv
[^/]+/Kindle\.Devices\.autoMarkAsRead\.zip
[^/]+/Kindle\.Devices\.autoMarkAsRead\.zip/Kindle\.Devices\.autoMarkAsRead\.csv
[^/]+/Kindle\.Devices\.kindleBookmarkActions\.zip
[^/]+/Kindle\.Devices\.kindleBookmarkActions\.zip/Kindle\.Devices\.kindleBookmarkActions\.csv
[^/]+/Kindle\.Devices\.kindleHighlightActions\.zip
[^/]+/Kindle\.Devices\.kindleHighlightActions\.zip/Kindle\.Devices\.kindleHighlightActions\.csv
[^/]+/Kindle\.Devices\.readerLibrarySeriesGroupingMetrics\.zip
[^/]+/Kindle\.Devices\.readerLibrarySeriesGroupingMetrics\.zip/Kindle\.Devices\.readerLibrarySeriesGroupingMetrics.csv
[^/]+/Kindle\.Devices\.showBookCoversOnAppResume\.zip
[^/]+/Kindle\.Devices\.showBookCoversOnAppResume\.zip/Kindle\.Devices\.showBookCoversOnAppResume\.csv
[^/]+/Kindle\.KindleContentUpdate\.zip
[^/]+/Kindle\.KindleContentUpdate\.zip/Kindle\.KindleContentUpdate
[^/]+/Kindle\.KindleContentUpdate\.zip/Kindle\.KindleContentUpdate/datasets
[^/]+/Kindle\.KindleContentUpdate\.zip/Kindle\.KindleContentUpdate/datasets/Kindle\.KindleContentUpdate\.ContentUpdates
[^/]+/Kindle\.KindleContentUpdate\.zip/Kindle\.KindleContentUpdate/datasets/Kindle\.KindleContentUpdate\.ContentUpdates/Kindle\.KindleContentUpdate\.ContentUpdates\.csv
[^/]+/Kindle\.KindleContentUpdate\.zip/Kindle\.KindleContentUpdate/datasets/Kindle\.KindleContentUpdate\.ManualContentUpdates
[^/]+/Kindle\.KindleContentUpdate\.zip/Kindle\.KindleContentUpdate/datasets/Kindle\.KindleContentUpdate\.ManualContentUpdates/Kindle\.KindleContentUpdate\.ManualContentUpdates\.csv
[^/]+/Kindle\.KindleDocs\.zip
[^/]+/Kindle\.KindleDocs\.zip/Kindle\.KindleDocs
[^/]+/Kindle\.KindleDocs\.zip/Kindle\.KindleDocs/datasets
[^/]+/Kindle\.KindleDocs\.zip/Kindle\.KindleDocs/datasets/Kindle\.KindleDocs\.KindleAliasToDeviceMapping
[^/]+/Kindle\.KindleDocs\.zip/Kindle\.KindleDocs/datasets/Kindle\.KindleDocs\.KindleAliasToDeviceMapping/Kindle\.KindleDocs\.KindleAliasToDeviceMapping\.csv
[^/]+/Kindle\.KindleHome\.CardViewsOnKindleForiOS\.zip
[^/]+/Kindle\.KindleHome\.CardViewsOnKindleForiOS\.zip/Kindle\.KindleHome\.CardViewsOnKindleForiOS\.csv
[^/]+/Kindle\.Reach\.zip
[^/]+/Kindle\.Reach\.zip/Kindle\.Reach
[^/]+/Kindle\.Reach\.zip/Kindle\.Reach/datasets
[^/]+/Kindle\.Reach\.zip/Kindle\.Reach/datasets/Kindle\.Reach\.KindleNotifications\.CustomerNotificationEvents
[^/]+/Kindle\.Reach\.zip/Kindle\.Reach/datasets/Kindle\.Reach\.KindleNotifications\.CustomerNotificationEvents/Kindle\.Reach\.KindleNotifications\.CustomerNotificationEvents\.csv
[^/]+/Kindle\.Reach\.zip/Kindle\.Reach/datasets/Kindle\.Reach\.KindleNotifications\.InAppNotifications
[^/]+/Kindle\.Reach\.zip/Kindle\.Reach/datasets/Kindle\.Reach\.KindleNotifications\.InAppNotifications/Kindle\.Reach\.KindleNotifications\.InAppNotifications\.csv
[^/]+/Kindle\.Reach\.zip/Kindle\.Reach/datasets/Kindle\.Reach\.KindleNotifications\.InappNotificationEvents
[^/]+/Kindle\.Reach\.zip/Kindle\.Reach/datasets/Kindle\.Reach\.KindleNotifications\.InappNotificationEvents/Kindle\.Reach\.KindleNotifications\.InappNotificationEvents\.csv
[^/]+/Kindle\.ReadingInsights\.zip
[^/]+/Kindle\.ReadingInsights\.zip/Kindle\.ReadingInsights
[^/]+/Kindle\.ReadingInsights\.zip/Kindle\.ReadingInsights/datasets
[^/]+/Kindle\.ReadingInsights\.zip/Kindle\.ReadingInsights/datasets/Kindle\.ReadingInsightsDayUnits
[^/]+/Kindle\.ReadingInsights\.zip/Kindle\.ReadingInsights/datasets/Kindle\.ReadingInsightsDayUnits/Kindle\.ReadingInsightsDayUnits\.csv
[^/]+/Kindle\.ReadingInsights\.zip/Kindle\.ReadingInsights/datasets/Kindle\.reading-insights-sessions_with_adjustments
[^/]+/Kindle\.ReadingInsights\.zip/Kindle\.ReadingInsights/datasets/Kindle\.reading-insights-sessions_with_adjustments/Kindle\.reading-insights-sessions_with_adjustments\.csv
[^/]+/Kindle\.SagaSeriesInfra\.zip
[^/]+/Kindle\.SagaSeriesInfra\.zip/Kindle\.SagaSeriesInfra
[^/]+/Kindle\.SagaSeriesInfra\.zip/Kindle\.SagaSeriesInfra/datasets
[^/]+/Kindle\.SagaSeriesInfra\.zip/Kindle\.SagaSeriesInfra/datasets/Kindle\.SagaSeriesInfra\.CollectionRightsDatastore
[^/]+/Kindle\.SagaSeriesInfra\.zip/Kindle\.SagaSeriesInfra/datasets/Kindle\.SagaSeriesInfra\.CollectionRightsDatastore/Kindle\.SagaSeriesInfra\.CollectionRightsDatastore\.csv
[^/]+/Kindle\.WebReader\.zip
[^/]+/Kindle\.WebReader\.zip/Kindle\.WebReader
[^/]+/Kindle\.WebReader\.zip/Kindle\.WebReader/datasets
[^/]+/Kindle\.WebReader\.zip/Kindle\.WebReader/datasets/Kindle\.WebReader\.lookInsideTheBook-\d+
[^/]+/Kindle\.WebReader\.zip/Kindle\.WebReader/datasets/Kindle\.WebReader\.lookInsideTheBook-\d+/Kindle\.WebReader\.lookInsideTheBook-\d+\.csv
[^/]+/OutboundNotifications\.AmazonApplicationUpdateHistory\.zip
[^/]+/OutboundNotifications\.AmazonApplicationUpdateHistory\.zip/OutboundNotifications\.AmazonApplicationUpdateHistory\.csv
[^/]+/OutboundNotifications\.EmailDeliveryStatusFeedback\.zip
[^/]+/OutboundNotifications\.EmailDeliveryStatusFeedback\.zip/OutboundNotifications\.EmailDeliveryStatusFeedback\.csv
[^/]+/OutboundNotifications\.NotificationEngagementEvents\.zip
[^/]+/OutboundNotifications\.NotificationEngagementEvents\.zip/OutboundNotifications\.NotificationEngagementEvents\.csv
[^/]+/OutboundNotifications\.PushSentData\.zip
[^/]+/OutboundNotifications\.PushSentData\.zip/OutboundNotifications\.PushSentData\.csv
[^/]+/OutboundNotifications\.SentNotifications\.zip
[^/]+/OutboundNotifications\.SentNotifications\.zip/OutboundNotifications\.SentNotifications\.csv
[^/]+/PaymentOptions\.PaymentInstruments\.zip
[^/]+/PaymentOptions\.PaymentInstruments\.zip/PaymentOptions\.PaymentInstruments\.csv
[^/]+/Payments\.PaymentProducts\.zip
[^/]+/Payments\.PaymentProducts\.zip/Payments\.PaymentProducts\.csv
[^/]+/PrimeVideo\.WatchEvent\.\d+\.\d+\.zip
[^/]+/PrimeVideo\.WatchEvent\.\d+\.\d+\.zip/PrimeVideo\.WatchEvent\.\d+\.\d+\.csv
[^/]+/PrimeVideo\.WatchEvent\.\d+\.\d+\.zip/PrimeVideoWatchEventDescription\.pdf
[^/]+/ProductCompatibilityProgram\.DataSet\.zip
[^/]+/ProductCompatibilityProgram\.DataSet\.zip/tmp
[^/]+/ProductCompatibilityProgram\.DataSet\.zip/tmp/[^/]+
[^/]+/ProductCompatibilityProgram\.DataSet\.zip/tmp/[^/]+/ProductCompatibility\.DataSet\.csv
[^/]+/Retail\.5MonthlyPayments\.zip
[^/]+/Retail\.5MonthlyPayments\.zip/Retail\.5MonthlyPayments
[^/]+/Retail\.5MonthlyPayments\.zip/Retail\.5MonthlyPayments/datasets
[^/]+/Retail\.5MonthlyPayments\.zip/Retail\.5MonthlyPayments/datasets/Retail\.5MonthlyPayments\.CreditCardEligibility
[^/]+/Retail\.5MonthlyPayments\.zip/Retail\.5MonthlyPayments/datasets/Retail\.5MonthlyPayments\.CreditCardEligibility/Retail\.5MonthlyPayments\.CreditCardEligibility\.csv
[^/]+/Retail\.5MonthlyPayments\.zip/Retail\.5MonthlyPayments/datasets/Retail\.5MonthlyPayments\.PastYearPurchaseDetails
[^/]+/Retail\.5MonthlyPayments\.zip/Retail\.5MonthlyPayments/datasets/Retail\.5MonthlyPayments\.PastYearPurchaseDetails/Retail\.5MonthlyPayments\.PastYearPurchaseDetails\.csv
[^/]+/Retail\.AddressAuthorityService\.zip
[^/]+/Retail\.AddressAuthorityService\.zip/Retail\.AddressAuthorityService
[^/]+/Retail\.AddressAuthorityService\.zip/Retail\.AddressAuthorityService/datasets
[^/]+/Retail\.AddressAuthorityService\.zip/Retail\.AddressAuthorityService/datasets/Retail\.AddressAuthorityService\.AddressIDDetails\.\d+
[^/]+/Retail\.AddressAuthorityService\.zip/Retail\.AddressAuthorityService/datasets/Retail\.AddressAuthorityService\.AddressIDDetails\.\d+/Retail\.AddressAuthorityService\.AddressIDDetails\.\d+\.csv
[^/]+/Retail\.AddressAuthorityService\.zip/Retail\.AddressAuthorityService/datasets/Retail\.AddressAuthorityService\.AddressIDSummary\.\d+
[^/]+/Retail\.AddressAuthorityService\.zip/Retail\.AddressAuthorityService/datasets/Retail\.AddressAuthorityService\.AddressIDSummary\.\d+/Retail\.AddressAuthorityService\.AddressIDSummary\.\d+\.csv
[^/]+/Retail\.Addresses\.zip
[^/]+/Retail\.Addresses\.zip/Retail\.Addresses
[^/]+/Retail\.Addresses\.zip/Retail\.Addresses/Retail\.Addresses\.pdf
[^/]+/Retail\.AmazonCustom\.\d+\.zip
[^/]+/Retail\.AmazonCustom\.\d+\.zip/Other-Data-Categories\.Amazon-Custom\.csv
[^/]+/Retail\.AmazonCustom\.\d+\.zip/Other-Data-Categories\.Amazon-Custom\.\d+\.jpg
[^/]+/Retail\.Authentication\.zip
[^/]+/Retail\.Authentication\.zip/Retail\.Authentication\.json
[^/]+/Retail\.AuthenticationTokens\.zip
[^/]+/Retail\.AuthenticationTokens\.zip/Retail\.AuthenticationTokens\.json
[^/]+/Retail\.AuthenticationTokens\.zip/description\.txt
[^/]+/Retail\.AutomotiveNA\.Garage\.zip
[^/]+/Retail\.AutomotiveNA\.Garage\.zip/Retail\.AutomotiveNA\.Garage\.csv
[^/]+/Retail\.AvatarData\.zip
[^/]+/Retail\.AvatarData\.zip/Avatar \d+
[^/]+/Retail\.AvatarData\.zip/Retail\.AvatarData\.pdf
[^/]+/Retail\.BuyerSellerMessaging\.zip
[^/]+/Retail\.BuyerSellerMessaging\.zip/Retail\.BuyerSellerMessaging
[^/]+/Retail\.BuyerSellerMessaging\.zip/Retail\.BuyerSellerMessaging/datasets
[^/]+/Retail\.BuyerSellerMessaging\.zip/Retail\.BuyerSellerMessaging/datasets/BuyerSellerMessaging
[^/]+/Retail\.BuyerSellerMessaging\.zip/Retail\.BuyerSellerMessaging/datasets/BuyerSellerMessaging/BuyerSellerMessaging\.csv
[^/]+/Retail\.CartItems\.\d+.zip
[^/]+/Retail\.CartItems\.\d+\.zip/Retail\.CartItems\.\d+\.csv
[^/]+/Retail\.ChatTranscripts\.zip
[^/]+/Retail\.ChatTranscripts\.zip/Retail.ChatTranscripts/datasets
[^/]+/Retail\.ChatTranscripts\.zip/Retail.ChatTranscripts/datasets/Retail\.ChatTranscripts\.MessageTranscriptsCampfire
[^/]+/Retail\.ChatTranscripts\.zip/Retail.ChatTranscripts/datasets/Retail\.ChatTranscripts\.MessageTranscriptsCampfire/Retail\.ChatTranscripts\.MessageTranscriptsCampfire\.csv
[^/]+/Retail\.ChatTranscripts\.zip/Retail.ChatTranscripts/media
[^/]+/Retail\.ChatTranscripts\.zip/Retail.ChatTranscripts/media/Retail\.ChatTranscripts\.MessageTranscriptsCampfire\.DSAR-WORKFLOW-[^/.]+\.pdf
[^/]+/Retail\.CommunityShopping\.SocialGamification\.zip
[^/]+/Retail\.CommunityShopping\.SocialGamification\.zip/Retail.CommunityShopping.SocialGamification/datasets
[^/]+/Retail\.CommunityShopping\.SocialGamification\.zip/Retail.CommunityShopping.SocialGamification/datasets/Retail\.CommunityShopping\.SocialGamification\.Stats
[^/]+/Retail\.CommunityShopping\.SocialGamification\.zip/Retail.CommunityShopping.SocialGamification/datasets/Retail\.CommunityShopping\.SocialGamification\.Stats/Retail\.CommunityShopping\.SocialGamification\.Stats\.csv
[^/]+/Retail\.CommunityTrust\.zip
[^/]+/Retail\.CommunityTrust\.zip/Retail\.CommunityTrust
[^/]+/Retail\.CommunityTrust\.zip/Retail\.CommunityTrust/datasets
[^/]+/Retail\.CommunityTrust\.zip/Retail\.CommunityTrust/datasets/Retail\.CommunityTrust\.CommunityContentContributionEligibility
[^/]+/Retail\.CommunityTrust\.zip/Retail\.CommunityTrust/datasets/Retail\.CommunityTrust\.CommunityContentContributionEligibility/Retail\.CommunityTrust\.CommunityContentContributionEligibility\.csv
[^/]+/Retail\.CustomerAttributes\.zip
[^/]+/Retail\.CustomerAttributes\.zip/Retail\.CustomerAttributes
[^/]+/Retail\.CustomerAttributes\.zip/Retail\.CustomerAttributes/Retail\.CustomerAttributes\.pdf
[^/]+/Retail\.CustomerContacts\.zip
[^/]+/Retail\.CustomerContacts\.zip/Retail\.CustomerContacts
[^/]+/Retail\.CustomerContacts\.zip/Retail\.CustomerContacts/Retail\.CustomerContacts\.pdf
[^/]+/Retail\.CustomerProfile\.zip
[^/]+/Retail\.CustomerProfile\.zip/datasets
[^/]+/Retail\.CustomerProfile\.zip/datasets/Retail\.CustomerProfile\.ProfileAttributes
[^/]+/Retail\.CustomerProfile\.zip/datasets/Retail\.CustomerProfile\.ProfileAttributes/Retail\.CustomerProfile\.ProfileAttributes\.csv
[^/]+/Retail\.CustomerQuestionsAndAnswers\.zip
[^/]+/Retail\.CustomerQuestionsAndAnswers\.zip/Retail.CustomerQuestionsAndAnswers/datasets
[^/]+/Retail\.CustomerQuestionsAndAnswers\.zip/Retail.CustomerQuestionsAndAnswers/datasets/Retail\.CustomerQuestionsAndAnswers\.AnswerNotificationRequests
[^/]+/Retail\.CustomerQuestionsAndAnswers\.zip/Retail.CustomerQuestionsAndAnswers/datasets/Retail\.CustomerQuestionsAndAnswers\.AnswerNotificationRequests/Retail\.CustomerQuestionsAndAnswers\.AnswerNotificationRequests\.csv
[^/]+/Retail\.CustomerQuestionsAndAnswers\.zip/Retail.CustomerQuestionsAndAnswers/datasets/Retail\.CustomerQuestionsAndAnswers\.QuestionsPosted
[^/]+/Retail\.CustomerQuestionsAndAnswers\.zip/Retail.CustomerQuestionsAndAnswers/datasets/Retail\.CustomerQuestionsAndAnswers\.QuestionsPosted/Retail\.CustomerQuestionsAndAnswers\.QuestionsPosted\.csv
[^/]+/Retail\.CustomerReturns\.\d+\.zip
[^/]+/Retail\.CustomerReturns\.\d+\.zip/Retail\.CustomerReturns\.\d+\.csv
[^/]+/Retail\.CustomerReviews\.zip
[^/]+/Retail\.CustomerReviews\.zip/Retail.CustomerReviews/datasets
[^/]+/Retail\.CustomerReviews\.zip/Retail.CustomerReviews/datasets/Retail\.CustomerReviews\.AmazonVerifiedPurchases\d+
[^/]+/Retail\.CustomerReviews\.zip/Retail.CustomerReviews/datasets/Retail\.CustomerReviews\.AmazonVerifiedPurchases\d+/Retail\.CustomerReviews\.AmazonVerifiedPurchases\d+\.csv
[^/]+/Retail\.CustomerReviews\.zip/Retail.CustomerReviews/datasets/Retail\.CustomerReviews\.ReviewsVersions\d+
[^/]+/Retail\.CustomerReviews\.zip/Retail.CustomerReviews/datasets/Retail\.CustomerReviews\.ReviewsVersions\d+/Retail\.CustomerReviews\.ReviewsVersions\d+\.csv
[^/]+/Retail\.CustomerServiceChatsContent\.\d+\.zip
[^/]+/Retail\.CustomerServiceChatsContent\.\d+\.zip/Retail\.CustomerServiceChatsContent\.\d+
[^/]+/Retail\.CustomerServiceChatsContent\.\d+\.zip/Retail\.CustomerServiceChatsContent\.\d+/Detail.CustomerServiceChatsContent\.\d+\.pdf
[^/]+/Retail\.CustomerServiceEmailsContent\.\d+\.zip
[^/]+/Retail\.CustomerServiceEmailsContent\.\d+\.zip/Retail\.CustomerServiceEmailsContent\.\d+
[^/]+/Retail\.CustomerServiceEmailsContent\.\d+\.zip/Retail\.CustomerServiceEmailsContent\.\d+/Retail.CustomerServiceEmailsContent\.\d+.pdf
[^/]+/Retail\.CustomerServiceFeedback\.\d+\.zip
[^/]+/Retail\.CustomerServiceFeedback\.\d+\.zip/Retail\.CustomerServiceFeedback\.\d+\.csv
[^/]+/Retail\.DigitalSoftwareAndVideogames\.Downloads\.\d+\.zip
[^/]+/Retail\.DigitalSoftwareAndVideogames\.Downloads\.\d+\.zip/Retail\.DigitalSoftwareAndVideogames\.Downloads\.csv
[^/]+/Retail\.DigitalSoftwareAndVideogames\.Entitlements\.\d+\.zip
[^/]+/Retail\.DigitalSoftwareAndVideogames\.Entitlements\.\d+\.zip/Retail\.DigitalSoftwareAndVideogames\.Entitlements\.\d+\.csv
[^/]+/Retail\.EarlyReviewerProgram\.zip
[^/]+/Retail\.EarlyReviewerProgram\.zip/Retail.EarlyReviewerProgram/datasets
[^/]+/Retail\.EarlyReviewerProgram\.zip/Retail.EarlyReviewerProgram/datasets/Retail\.EarlyReviewerProgram\.\d+
[^/]+/Retail\.EarlyReviewerProgram\.zip/Retail.EarlyReviewerProgram/datasets/Retail\.EarlyReviewerProgram\.\d+/Retail\.EarlyReviewerProgram\.\d+\.csv
[^/]+/Retail\.GiftCertificates\.\d+\.zip
[^/]+/Retail\.GiftCertificates\.\d+\.zip/Retail\.GiftCertificates\.Transactions\.pdf
[^/]+/Retail\.Household\.zip
[^/]+/Retail\.Household\.zip/Retail\.Household
[^/]+/Retail\.Household\.zip/Retail\.Household/Retail\.Household\.pdf
[^/]+/Retail\.LightWeightInteractions\.zip
[^/]+/Retail\.LightWeightInteractions\.zip/Retail\.LightWeightInteractions
[^/]+/Retail\.LightWeightInteractions\.zip/Retail\.LightWeightInteractions/datasets
[^/]+/Retail\.LightWeightInteractions\.zip/Retail\.LightWeightInteractions/datasets/LightWeightAggregates
[^/]+/Retail\.LightWeightInteractions\.zip/Retail\.LightWeightInteractions/datasets/LightWeightAggregates/LightWeightAggregates\.csv
[^/]+/Retail\.LightWeightInteractions\.zip/Retail\.LightWeightInteractions/datasets/LightWeightAggregates/LightWeightAggregates_\d+\.csv
[^/]+/Retail\.LightWeightInteractions\.zip/Retail\.LightWeightInteractions/datasets/LightWeightInteractions
[^/]+/Retail\.LightWeightInteractions\.zip/Retail\.LightWeightInteractions/datasets/LightWeightInteractions/LightWeightInteractions\.csv
[^/]+/Retail\.OrderHistory\.\d+\.zip
[^/]+/Retail\.OrderHistory\.\d+\.zip/Retail\.OrderHistory\.\d+\.csv
[^/]+/Retail\.Orders\.ManageYourReturns\.\d+\.zip
[^/]+/Retail\.Orders\.ManageYourReturns\.\d+\.zip/Retail\.Orders\.ManageYourReturns\.\d+\.csv
[^/]+/Retail\.OrdersReturned\.\d+\.zip
[^/]+/Retail\.OrdersReturned\.\d+\.zip/Retail\.OrdersReturned\.\d+\.csv
[^/]+/Retail\.OrdersReturned\.Payments\.\d+\.zip
[^/]+/Retail\.OrdersReturned\.Payments\.\d+\.zip/Retail\.OrdersReturned\.Payments\.\d+\.csv
[^/]+/Retail\.OutboundNotifications\.zip
[^/]+/Retail\.OutboundNotifications\.zip/Retail.OutboundNotifications/datasets
[^/]+/Retail\.OutboundNotifications\.zip/Retail.OutboundNotifications/datasets/Retail\.OutboundNotifications\.MobileApplications
[^/]+/Retail\.OutboundNotifications\.zip/Retail.OutboundNotifications/datasets/Retail\.OutboundNotifications\.MobileApplications/Retail\.OutboundNotifications\.MobileApplications\.csv
[^/]+/Retail\.OutboundNotifications\.zip/Retail.OutboundNotifications/datasets/Retail\.OutboundNotifications\.notificationMetadata
[^/]+/Retail\.OutboundNotifications\.zip/Retail.OutboundNotifications/datasets/Retail\.OutboundNotifications\.notificationMetadata/Retail\.OutboundNotifications\.notificationMetadata\.csv
[^/]+/Retail\.OutboundNotifications\.zip/Retail.OutboundNotifications/datasets/Retail\.OutboundNotifications\.notificationMetadata/Retail\.OutboundNotifications\.notificationMetadata_\d+\.csv
[^/]+/Retail\.ProfileData\.zip
[^/]+/Retail\.ProfileData\.zip/Retail\.ProfileData.pdf
[^/]+/Retail\.Promotions\.\d+\.zip
[^/]+/Retail\.Promotions\.\d+\.zip/Retail\.Promotions\.csv
[^/]+/Retail\.RegionAuthority\.\d+\.zip
[^/]+/Retail\.RegionAuthority\.\d+\.zip/Retail\.RegionAuthority\.\d+\.csv
[^/]+/Retail\.Reorder\.\d+\.zip
[^/]+/Retail\.Reorder\.\d+\.zip/Retail\.Reorder\.\d+
[^/]+/Retail\.Reorder\.\d+\.zip/Retail\.Reorder\.\d+/Retail\.Reorder\.DigitalDashSummary\.csv
[^/]+/Retail\.Reorder\.\d+\.zip/Retail\.Reorder\.\d+/Retail\.Reorder\.DigitalDashCustomerPreference\.csv
[^/]+/Retail\.Reorder\.\d+\.zip/Retail\.Reorder\.\d+/Retail\.Reorder\.DigitalDashButton\.csv
[^/]+/Retail\.Returns\.Receivable\.\d+\.zip
[^/]+/Retail\.Returns\.Receivable\.\d+\.zip/Retail\.Returns\.Receivable\.\d+\.csv
[^/]+/Retail\.Returns\.Retrocharge\.\d+\.zip
[^/]+/Retail\.Returns\.Retrocharge\.\d+\.zip/Retail\.Returns\.Retrocharge\.\d+\.csv
[^/]+/Retail\.Search-Data\.zip
[^/]+/Retail\.Search-Data\.zip/Retail\.Search-Data\.Retail\.Customer-Engagement\.csv
[^/]+/Retail\.Search-Data\.zip/Retail\.Search-Data\.Retail\.Product-Metrics\.csv
[^/]+/Retail\.Seller-Feedback\.\d+\.zip
[^/]+/Retail\.Seller-Feedback\.\d+\.zip/Retail\.Seller-Feedback\.csv
[^/]+/Retail\.ShopperPanel\.\d+\.\d+\.zip
[^/]+/Retail\.ShopperPanel\.\d+\.\d+\.zip/Retail\.ShopperPanel\.Profile\.csv
[^/]+/Retail\.ShoppingProfile\.\d+\.zip
[^/]+/Retail\.ShoppingProfile\.\d+\.zip/Retail\.ShoppingProfile\.\d+\.csv
[^/]+/Retail\.TaxExemptions\.zip
[^/]+/Retail\.TaxExemptions\.zip/Retail\.TaxExemptions\.ExemptionsCertificate\.csv
[^/]+/Retail\.TransactionalInvoicing\.\d+\.zip
[^/]+/Retail\.TransactionalInvoicing\.\d+\.zip/Retail\.TransactionalInvoicing\.pdf
[^/]+/SearchHistory\.Images\.ClickData\.zip
[^/]+/SearchHistory\.Images\.ClickData\.zip/SearchHistory\.Images\.ClickData\.ClickStream\.CSV
[^/]+/Subscriptions\.Digital-Billing-And-Refunds\.\d+\.zip
[^/]+/Subscriptions\.Digital-Billing-And-Refunds\.\d+\.zip/Billing and Refunds Data\.csv
[^/]+/Subscriptions\.Digital-Billing-And-Refunds\.\d+\.zip/README\.txt
[^/]+/Subscriptions\.Periodicals\.PrintMagazines\.zip
[^/]+/Subscriptions\.Periodicals\.PrintMagazines\.zip/datasets
[^/]+/Subscriptions\.Periodicals\.PrintMagazines\.zip/datasets/Periodicals\.PrintMagazineSubscriptions\.Subscriptions
[^/]+/Subscriptions\.Periodicals\.PrintMagazines\.zip/datasets/Periodicals\.PrintMagazineSubscriptions\.Subscriptions/Periodicals\.PrintMagazineSubscriptions\.Subscriptions\.csv
[^/]+/Subscriptions\.Periodicals\.PrintMagazines\.zip/datasets/Periodicals\.PrintMagazineSubscriptions\.SubscriptionChangeHistory
[^/]+/Subscriptions\.Periodicals\.PrintMagazines\.zip/datasets/Periodicals\.PrintMagazineSubscriptions\.SubscriptionChangeHistory/Periodicals\.PrintMagazineSubscriptions\.SubscriptionChangeHistory\.csv
[^/]+/Subscriptions\.RecurringDeliveries\.zip
[^/]+/Subscriptions\.RecurringDeliveries\.zip/datasets
[^/]+/Subscriptions\.RecurringDeliveries\.zip/datasets/Subscriptions\.RecurringDeliveries\.Events
[^/]+/Subscriptions\.RecurringDeliveries\.zip/datasets/Subscriptions\.RecurringDeliveries\.Events/Subscriptions\.RecurringDeliveries\.Events\.csv
[^/]+/Subscriptions\.RecurringDeliveries\.zip/datasets/Subscriptions\.RecurringDeliveries\.Orders
[^/]+/Subscriptions\.RecurringDeliveries\.zip/datasets/Subscriptions\.RecurringDeliveries\.Orders/Subscriptions\.RecurringDeliveries\.Orders\.csv
[^/]+/Subscriptions\.RecurringDeliveries\.zip/datasets/Subscriptions\.RecurringDeliveries\.Subscriptions
[^/]+/Subscriptions\.RecurringDeliveries\.zip/datasets/Subscriptions\.RecurringDeliveries\.Subscriptions/Subscriptions\.RecurringDeliveries\.Subscriptions\.csv
