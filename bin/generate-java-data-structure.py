#!/usr/bin/python
#
# Emit, to stdout, the Java data structure given the Amazon .zip files in the current directory
# This needs manual cleanup afterwards: remove methods not currently implemented, and
# change file names into regexes in case there is an open list of them, e.g.
# Advertising.1.zip becomes advertising.\d+.zip

import os
import re
import zipfile

skipList = list( map( lambda x : re.compile( x ), [
    '.*\.eml',
    'KindleEBook_.*',
    'KindleEBookSample_.*',
    'KindleMagazine_.*'
] ))

print( "    public static final FileEntryRecord [] EXPECTED_FILES = {" )

for f in os.listdir( '.' ) :
    if f.endswith( '.zip' ) :
        try :
            zip = zipfile.ZipFile( f, 'r' )
            print(  '        fileEntryRecord(' )
            print( f'               "{ f }"', end='' )

            for entry in zip.namelist() :
                doSkip = False
                for skip in skipList :
                    if skip.match( entry ) :
                        doSkip = True

                if not doSkip :
                    handler = f[0:-4] + '.' + entry
                    handler = "".join( map( lambda x : x[0].capitalize() + x[1:], re.split( '[-\./_ ]+', handler )))
                    handler = "handle" + handler
                    print( ',' )
                    print( f'               fileEntry( "{ entry }", AmazonParser::{ handler } )', end='' )

            zip.close()
            print( ' ),' )

        except zipfile.BadZipFile:
            pass

print( "    };" )

